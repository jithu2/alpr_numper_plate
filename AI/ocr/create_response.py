import re 
from api.apivin import via_find
from AI.ocr.text_extracter import extract_text
import cv2
import numpy as np
import base64
import time
import traceback

def create_response_json(x1,y1,x2,y2,im0,deviceId,plate_data,json_sent_data,temp,stolen_list,ocr,stolen_list_number_plate,state_code):
    try:
        cropped_img = im0[y1:y2, x1:x2,:]
        numpy_image = cv2.cvtColor(cropped_img, cv2.COLOR_BGR2GRAY)
        ima2 = cv2.resize(numpy_image, (320, 163)).astype(np.float32)
        rotated_image = cv2.rotate(ima2, cv2.ROTATE_180)
                                
                                        
        
        # Create a mask with the same size as the rotated image
        # Create a mask with the same size as the rotated image
        ImageOneConvertStartTime=time.time()
        _, image_data = cv2.imencode(".jpg", im0)
        
        # Encode the image data to base64
        image_base64 = base64.b64encode(image_data).decode("utf-8")
        ImageOneConvertEndTime=time.time()
        
        # Convert the image to JPEG format
        ImageTwoConvertStartTime=time.time()
        _, image_data_plate = cv2.imencode(".jpg", cropped_img)
        # Encode the image data to base64
        image_base64_plate= base64.b64encode(image_data_plate).decode("utf-8")
        ImageTwoConvertEndtTime=time.time()


        text=extract_text(rotated_image,ocr)
        if text!="not detected" and text is not None:
           
            if deviceId in plate_data:
                print("#############################################",deviceId,"#######",len(plate_data[deviceId]))
            
            if text is not None and len(text) < 5:
                pass
            elif len(text)<len(temp) and text in temp:
                pass
            elif len(text)==len(temp) and text==temp:
                pass
            else:
            
                temp=text
                

                if text is not None and len(text)>0:
                    
                    if deviceId not in plate_data.keys():
                        plate_data[deviceId]=[text]      
                    else:
                        value=plate_data[deviceId]
                        value.append(text)
                        plate_data[deviceId]=value
                    if  text in stolen_list:
                        text=re.sub(r'[^a-zA-Z0-9]', '',text)
                        print("original text:     ",text)
                        
                        api_data=via_find(state_code,text)
                        
                        if api_data["success"]:
                            print("api success")
                            data=api_data["vin"]
                            print("api data=",data)
                            vin=data["vin"]
                            year=data["year"]
                            make=data["make"]
                            car_model=data["model"]
                            color=data["color"]["name"]
                            json_sent_data["make"]=data["make"]
                            json_sent_data["model"]=data["model"]
                            json_sent_data["color"]=data["color"]["name"]
                        json_sent_data["stolen"]=1
                        if deviceId not in stolen_list_number_plate.keys():
                            
                            stolen_list_number_plate[deviceId]=[text]
                            print("added numberplate to stolen list")
                        else:
                            value_stolen=stolen_list_number_plate[deviceId]
                            value_stolen.append(text)
                            stolen_list_number_plate[deviceId]=value_stolen
                            
                            
                    else:
                        json_sent_data["stolen"]=0
                    
                    json_sent_data["stolen_count"]= int(len(set(stolen_list_number_plate[deviceId]))) 
                    json_sent_data["plate"]=plate_data[deviceId][-1]   
                    json_sent_data["total_count"]= int(len(set(plate_data[deviceId]))) 
                    json_sent_data["image"]=image_base64
                    json_sent_data["image_number_plate"]=image_base64_plate

                    return json_sent_data,temp,True

        return json_sent_data,temp,False
    except Exception as e:
        print(e)
        traceback.print_exc()
                

                