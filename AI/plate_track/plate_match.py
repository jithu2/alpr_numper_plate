import cv2
import os
import numpy as np
import traceback
import shutil

def image_similarity(img1, img2, match_threshold=0.9):
    try:
        # # Load the two images
        # img1 = cv2.imread(img1)
        # img2 = cv2.imread(img2)

        # Convert images to grayscale
        img1_gray = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
        img2_gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)

        # Create an ORB detector
        orb = cv2.ORB_create()

        # Find the key points and descriptors with ORB
        keypoints1, descriptors1 = orb.detectAndCompute(img1_gray, None)
        keypoints2, descriptors2 = orb.detectAndCompute(img2_gray, None)

        # Create a BFMatcher (Brute-Force Matcher) object
        bf = cv2.BFMatcher(cv2.NORM_HAMMING, crossCheck=True)

        # Match descriptors
        matches = bf.match(descriptors1, descriptors2)

        # Sort them in ascending order of distance
        matches = sorted(matches, key=lambda x: x.distance)

        # Calculate the match ratio
        if  min(len(keypoints1), len(keypoints2))!=0:

            match_ratio = len(matches) / min(len(keypoints1), len(keypoints2))

            return match_ratio >= match_threshold
        else:
            return True
    except Exception as e:
        # print(e)
        traceback.print_exc()