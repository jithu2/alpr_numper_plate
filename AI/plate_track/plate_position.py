def check_nmuber_plate_position(x1,y1,x2,y2):
    try:

        height, width, channels = 640,480,3
        desired_width = width * 0.5  # 50% of the width
        desired_height = height * 0.5  # 50% of the height

        center_x_min = (width - desired_height) / 2
        center_x_max = (width + desired_height) / 2
        center_y_min = 0
        center_y_max = height


        center_x = (x1 + (x2-x1)) // 2
        center_y = (y1 + (y2-y1)) // 2
        if center_x_min <= center_x <= center_x_max and center_y_min <= center_y <= center_y_max:
            # Perform an action when the number plate is in the center
            print("Number plate is in the center. Proceed with the action.")
            return "Centre"
        else:
            print("frame is not in centre")
            return "not in centre"
    except Exception as e:
        print(e)