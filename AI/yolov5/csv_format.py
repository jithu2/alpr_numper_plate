import csv
import datetime

# Create a list of data to write to the CSV file
data = [
    ['Hello', 'World', datetime.datetime.now()],
    ['Goodbye', 'Moon', datetime.datetime.now()]
]

# Open a new CSV file in write mode and create a CSV writer object
with open('example.csv', 'w', newline='') as file:
    writer = csv.writer(file)

    # Write the header row
    writer.writerow(['Text 1', 'Text 2', 'Timestamp'])

    # Write each row of data
    for row in data:
        writer.writerow(row)
