
import argparse
import os
import platform
import sys
from pathlib import Path
import numpy as np
import torch
import requests
import csv
import datetime
import time
import traceback
import asyncio
import json
import websockets
from websockets.server import serve
# from socket_server import send_plate_data_to_client
from logger.logger_format import init_logger
import cv2
import base64
import json
import re
import threading
import asyncio
import json
from  AI.ocr.create_response import create_response_json
from AI.plate_track.plate_match import image_similarity

FILE = Path(__file__).resolve()
ROOT = FILE.parents[0]  # YOLOv5 root directory
if str(ROOT) not in sys.path:
    sys.path.append(str(ROOT))  # add ROOT to PATH
ROOT = Path(os.path.relpath(ROOT, Path.cwd()))  # relative

from models.common import DetectMultiBackend
from utils.dataloaders import IMG_FORMATS, VID_FORMATS, LoadImages, LoadScreenshots, LoadStreams
from utils.general import (LOGGER, Profile, check_file, check_img_size, cv2,
                           increment_path, non_max_suppression, scale_boxes)
from utils.plots import Annotator
from utils.torch_utils import select_device, smart_inference_mode
from api.apivin import via_find
from AI.plate_track.plate_position import check_nmuber_plate_position



def load_model(weights,data):
    model = DetectMultiBackend(weights, device=torch.device('cpu'), dnn=True, data=data, fp16=False)
    stride, names, pt = model.stride, model.names, model.pt
    return model,stride,names,pt


@smart_inference_mode()
def run(
        model,stride,names,pt,device,ocr,plate_data,websocket,send_plate_data_to_client,state_code,state_name,closed_websockets,web_socket_data,previous_frame,frame_prepeat_check,

        source,
        deviceId="",
        stolen_list_number_plate={},
        connection="",
        stolen_list=[],
        plate_list=[],
        imgsz=(640, 640), 
        conf_thres=0.1, 
        iou_thres=0.01,  
        max_det=1000,  
        save_crop=False,  
        
        classes=None,  
        agnostic_nms=False,  
        augment=False,  
        visualize=False,  
        line_thickness=3,  
        vid_stride=1
):
  
    source = str(source)
    frame_count=0
    previous_track_id="" 
    try:
        cap = cv2.VideoCapture(source)
        
        while not cap.isOpened():
            try:
                if websocket.closed:
                    print("wouza not working")
                    break
                cap = cv2.VideoCapture(source)
                print("no frame detected")
                
            except Exception as e:
                print(e)
                traceback.print_exc()
    except Exception as e:
        print("error occured : ",e)
        traceback.print_exc()

    if websocket.closed:
        print("wouza not working")
    else:
        while cap.isOpened():
            ret, frame = cap.read()
            if ret:
                print(type(frame))
                break
        print(state_name)
        json_sent_data =  {
                            "plate": "KL9094",
                            "stolen_count": 2,
                            "total_count": 5,
                            "stolen": 0,
                            "state" : state_name,
                            "make" : "Mercedes",
                            "model" : "C220",
                            "color" : "Gray",
                            "date_reported" : "01/24/2023",
                            "image":"",
                            "image_number_plate":""
                        } 
            
        # Do something while the capture device is not open
        # For example, you could print a message or wait for the device to become available

        temp=""
        stream_start_time=time.time()

        is_file = Path(source).suffix[1:] in (IMG_FORMATS + VID_FORMATS)

        is_url = source.lower().startswith(('rtsp://', 'rtmp://', 'http://', 'https://'))
        webcam = source.isnumeric() or source.endswith('.streams') or (is_url and not is_file)
        screenshot = source.lower().startswith('screen')
        if is_url and is_file:
            source = check_file(source)  # download
            

        # # Directories
        # save_dir = increment_path("output", exist_ok=exist_ok)  # increment run
        # (save_dir / 'labels' if save_txt else save_dir).mkdir(parents=True, exist_ok=True)  # make dir

        # Load model
        device = select_device(device)
        # model = DetectMultiBackend(weights, device=device, dnn=dnn, data=data, fp16=half)
        # stride, names, pt = model.stride, model.names, model.pt
        
        imgsz = check_img_size(imgsz, s=stride)  # check image size

        # Dataloader
        bs = 1  # batch_size
        if webcam:
            dataset = LoadStreams(source, img_size=imgsz, stride=stride, auto=pt, vid_stride=vid_stride)
            print("stream checked")
            bs = len(dataset)
        elif screenshot:
            dataset = LoadScreenshots(source, img_size=imgsz, stride=stride, auto=pt)
        else:
            dataset = LoadImages(source, img_size=imgsz, stride=stride, auto=pt, vid_stride=vid_stride)
        vid_path, vid_writer = [None] * bs, [None] * bs

        # Run inference
        inference_start_time=time.time()
        temperory_time=0
        # model.warmup(imgsz=(1 if pt or model.triton else bs, 3, *imgsz))  # warmup
        seen, windows, dt = 0, [], (Profile(), Profile(), Profile())

        print("detect.py start .......")
        print(dataset)
        previous_frame[deviceId]={
            "old_frame":[],
            "new_frame":[]
        }
        print(previous_frame[deviceId]["old_frame"])
        frame_prepeat_check[deviceId]={
            "old_frame":[],
            "new_frame":[]
        }
    
        for path, im, im0s, vid_cap, s in dataset:
            
            





            start_time_each=time.time()
            
            try:
                if websocket.closed and len(web_socket_data)>0:
                    print("socket closed")
                    end_time_each=time.time()
                    time_for_model=end_time_each-start_time_each
                    print("single mmobile excecution time===",time_for_model)
                    # device_name = [key for key, val in web_socket_data.items() if val == websocket][0]
                    for key, val in web_socket_data.items():
                        if val == websocket:
                            device_name=key
                            print("plate_data=",plate_data)
                            if device_name in plate_data:
                                del plate_data[device_name]
                                print(plate_data)
                            if device_name in stolen_list_number_plate:
                                del stolen_list_number_plate[device_name]
                                print(stolen_list_number_plate)

                            print("matching_keys=",device_name)
                        else:
                            device_name="empty"



                    
                    
                    break
                stream_end_time=time.time()
                time_diff=stream_end_time - stream_start_time
                frame_count+=1
                stream_start_time=stream_end_time


                frame_prepeat_check[deviceId]["new_frame"]=[im0s[0]]

                if len(frame_prepeat_check[deviceId]["old_frame"])!=len(frame_prepeat_check[deviceId]["new_frame"]):
                    frame_prepeat_check[deviceId]["old_frame"]=frame_prepeat_check[deviceId]["new_frame"]
                else:
                    
                    for each in frame_prepeat_check[deviceId]["new_frame"]:
                        new_frame=each
                        old_frame=frame_prepeat_check[deviceId]["old_frame"][0]
                    
                        
                        
                        
                        similar_frame=image_similarity(old_frame,new_frame,0.9)
                        if similar_frame:
                            pass
                        else:
                            print("start yolo prediction ......................")


                            frame_count=0
                            log=init_logger("INFO")
                            
                            
                            with dt[0]:

                                im = torch.from_numpy(im).to(model.device)
                                im = im.half() if model.fp16 else im.float()  # uint8 to fp16/32
                                im /= 255  # 0 - 255 to 0.0 - 1.0
                                if len(im.shape) == 3:
                                    im = im[None]  # expand for batch dim

                            # Inference
                            with dt[1]:
                                start_time_for_model_prediction_of_ech_frame=time.time()
                                visualize = increment_path(save_dir / Path(path).stem, mkdir=True) if visualize else False
                                pred = model(im, augment=augment, visualize=visualize)

                            # NMS
                            with dt[2]:
                                pred = non_max_suppression(pred, conf_thres, iou_thres, classes, agnostic_nms, max_det=max_det)

                            # Second-stage classifier (optional)
                            # pred = utils.general.apply_classifier(pred, classifier_model, im, im0s)

                            # Process predictions

                            if len(pred[0])>0:
                            
                                for i, det in enumerate(pred):  # per image
                                    seen += 1
                                    if webcam:  # batch_size >= 1
                                        p, im0, frame = path[i], im0s[i].copy(), dataset.count
                                        s += f'{i}: '
                                    else:
                                        p, im0, frame = path, im0s.copy(), getattr(dataset, 'frame', 0)

                                    p = Path(p)  # to Path
                                    # save_path = str(save_dir / p.name)  # im.jpg
                                    # txt_path = str(save_dir / 'labels' / p.stem) + ('' if dataset.mode == 'image' else f'_{frame}')  # im.txt
                                    s += '%gx%g ' % im.shape[2:]  # print string
                                    gn = torch.tensor(im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
                                    imc = im0.copy() if save_crop else im0  # for save_crop
                                    annotator = Annotator(im0, line_width=line_thickness, example=str(names))


                                    if len(det):
                                        # Rescale boxes from img_size to im0 size
                                        det[:, :4] = scale_boxes(im.shape[2:], det[:, :4], im0.shape).round()

                                        # Print results
                                        for c in det[:, 5].unique():
                                            n = (det[:, 5] == c).sum()  # detections per class
                                            s += f"{n} {names[int(c)]}{'s' * (n > 1)}, "  # add to string

                                        # Write results

                                        track_data_list=[]
                                        for *xyxy, conf, cls in reversed(det):

                                            print("confidence=",conf)
                                            
                                    
                                            

                                            
                                            if int(cls)==0:
                                                
                                                x1=int(xyxy[0])
                                                y1=int(xyxy[1])
                                                x2=int(xyxy[2])
                                                y2=int(xyxy[3])
                                                cropped_img = im0[y1:y2, x1:x2,:]
                                                # previous_frame[deviceID]["new frame"]=track_data_list
                                                track_data_list.append(cropped_img)


                                previous_frame[deviceId]["new_frame"]=track_data_list
                        
                                


                                if len(previous_frame[deviceId]["old_frame"])!=len(previous_frame[deviceId]["new_frame"]):
                                    previous_frame[deviceId]["old_frame"]=previous_frame[deviceId]["new_frame"]
                                else:
                                    image_counter=0
                                    for each in previous_frame[deviceId]["new_frame"]:
                                        new_number_plate=each
                                        old_number_plate=previous_frame[deviceId]["old_frame"][image_counter]
                                        image_counter+=1
                                        similarity=image_similarity(old_number_plate,new_number_plate,0.9)
                                        if similarity:
                                            pass

                                        else:

                                            posittion_of_plate=check_nmuber_plate_position(x1,y1,x2,y2)
                                            if posittion_of_plate == "Centre":
                                                print(temp)
                                                json_sent_data,temp,check=create_response_json(x1,y1,x2,y2,im0,deviceId,plate_data,json_sent_data,temp,stolen_list,ocr,stolen_list_number_plate,state_code)
                                                if check:

                                                    print("json_sent_data",json_sent_data["plate"])
                                                    print("i am going to send data")

                                                    if deviceId in web_socket_data:
                                                        connection=asyncio.new_event_loop().run_until_complete(send_plate_data_to_client(json_sent_data, web_socket_data,deviceId))
                                    
                                            else:
                                                print("numper plate not in centre")
                                                print("i will not sent data")
                                    previous_frame[deviceId]["old_frame"]=previous_frame[deviceId]["new_frame"]

                    frame_prepeat_check[deviceId]["old_frame"]=frame_prepeat_check[deviceId]["new_frame"]             
                inference_end_time=time.time()
                check_each_second=inference_end_time-inference_start_time
                if int(check_each_second)>temperory_time:
                    temperory_time=check_each_second
                    # print("each second ..............=====",temperory_time)

            except Exception as e:
                print(f"An error occurred: {e}")
                traceback.print_exc() 
            except RuntimeError as e:
                # Handle the error
                print(f"An error occurred: {e}")
                traceback.print_exc()
                               
                            
                
