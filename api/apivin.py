import requests
import json
import time
from config.config import *




def via_find(state_code,plate_number):
    url = vin_api_url
    payload = {
        "state": state_code,
        "plate": plate_number
    }
    headers = {
    'Authorization': api_token,
    'Content-Type': 'application/json',
    'Accept': 'application/json'
    
    }
    response = requests.request('POST', url, headers=headers, json=payload)
    data=response.json()
    return data


def vin_advanced(vin):
    

    url = vin_advanced_url
    payload = {
        "vin": vin
    }
    headers = {
    'Authorization': api_token,
    'Content-Type': 'application/json',
    'Accept': 'application/json'
    
    }

    response = requests.request('POST', url, headers=headers, json=payload)
    data=response.json()
    return data



