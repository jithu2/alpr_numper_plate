from dotenv import load_dotenv
import os

# Load environment variables from .env file
load_dotenv()

# Access database configuration variables
db_host = os.getenv("DB_HOST")
db_name = os.getenv("DB_NAME")
db_user = os.getenv("DB_USER")
db_password = os.getenv("DB_PASSWORD")
db_port = os.getenv("DB_PORT")

# Access API configuration variables
api_token = os.getenv("API_TOKEN")
vin_api_url = os.getenv("VIN_API_URL")
vin_advanced_url= os.getenv("vin_advanced_url")


# ec2 instance 
server=os.getenv("server")
port=os.getenv("port")

#yolo parameter
vid_stride=int(os.getenv("vid_stride"))
conf_thres=float(os.getenv("conf_thres"))
iou_thres=float(os.getenv("iou_thres"))