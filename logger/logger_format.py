import logging
import datetime

def init_logger(log_level):
    # Step 1: Create a logger instance
    logger = logging.getLogger("my_logger")
    
    # Step 2: Set the logging level to the lowest level you want to record.
    logger.setLevel(logging.DEBUG)  # We set it to DEBUG to capture all levels of logs.
    
    # Step 3: Create a formatter to specify the log message format
    formatter = logging.Formatter(f"{log_level}: %(message)s - DT: %(asctime)s")
    
    # Get the current timestamp
    current_timestamp = datetime.datetime.now().strftime("%Y-%m-%d")
    
    # Step 4: Create a FileHandler to write logs to a file
    file_handler = logging.FileHandler(f"log_file/log_file.log")
    file_handler.setFormatter(formatter)
    
    # Step 5: Add the FileHandler to the logger
    logger.addHandler(file_handler)

    # Step 6: Create a StreamHandler to log messages to the console as well
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(formatter)

    # Step 7: Add the StreamHandler to the logger
    logger.addHandler(console_handler)
    
    return logger

# # Call the function to initialize the logger with the log level as "INFO"
# info_logger = init_logger("INFO")

# # Start logging messages using "INFO" level
# info_logger.info("This is an info message.")
# info_logger.error("This is an error message.")

# # Call the function to initialize the logger with the log level as "ERROR"
# error_logger = init_logger("ERROR")

# # Start logging messages using "ERROR" level
# error_logger.info("This is an info message.")
# error_logger.error("This is an error message.")
