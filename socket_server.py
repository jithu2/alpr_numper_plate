import asyncio
import json
import time
from websockets.server import serve
from AI.yolov5.detect import load_model, run
from AI.ocr.PaddleOCR.paddleocr import PaddleOCR
import threading
import websockets
from logger.logger_format import init_logger
from database.db_connection import get_stolen_number_plate_list
from config.config import *
import traceback


start_time_for_model_loading = time.time()

log=init_logger("INFO")

model, stride, names, pt = load_model(weights="AI/yolo_weight/yolo_detection.pt", data="AI/yolov5/data/coco.yaml")
ocr = PaddleOCR(det_model_dir="AI/ocr/ch_ppocr_mobile_v2.0_det_infer", rec_model_dir="AI/ocr/ch_ppocr_mobile_v2.0_rec_infer", cls_model_dir="AI/ocr/ch_ppocr_mobile_v2.0_cls_infer", use_angle_cls=True, lang='en')
server = server
port = port
plate_data = {}
web_socket_data = {}
previous_frame={}
frame_prepeat_check={}
end_time_for_model_loading = time.time()
execution_time_for_model_loading = end_time_for_model_loading - start_time_for_model_loading
stolen_list=get_stolen_number_plate_list()
print(f"Execution time for model loading= {execution_time_for_model_loading}")
log.info(f"Execution time for model loading= {execution_time_for_model_loading}")
stolen_list_number_plate={}
closed_websockets=[]
# Load data from the JSON file
with open('config/state.json', 'r') as json_file:
    state_data = json.load(json_file)


async def send_plate_data_to_client(plate_data, web_socket_data, deviceId):
    try:
        # Check if the deviceId exists in the dictionary
        if deviceId in web_socket_data:
            websocket = web_socket_data[deviceId]
            # await websocket.ping()
            if not websocket.closed:

                await websocket.send(json.dumps(plate_data))
                return "running"
        else:
            print(f"Device ID '{deviceId}' not found in web_socket_data.")
            return "closed"
    except websockets.exceptions.ConnectionClosedOK:
        print("Websocket connection is already closed.")
        traceback.print_exc()
        return "closed"
        # Handle the closed connection, perform cleanup, or notify clients
    except Exception as e:
        print("the error in deviceID",deviceId,"plate_data=",plate_data["plate"])
        traceback.print_exc()
        print(e)
        return "closed"


async def testHandler(websocket):
    try:
        async for message in websocket:
            payload = json.loads(message)
            deviceId = payload["deviceId"]
            streamid = payload["streamUrl"]
            
            # state_code=payload["stateCode"]
            state_code="VA"

            if state_code in state_data.keys():
                state_name=state_data[state_code]
            web_socket_data[deviceId] = websocket
            stolen_list_number_plate[deviceId]=[]
            kwargs={
                        "closed_websockets":closed_websockets,
                        "frame_prepeat_check":frame_prepeat_check,
                        "web_socket_data":web_socket_data,
                        "state_name":state_name,
                        "state_code":state_code,
                        "stolen_list_number_plate":stolen_list_number_plate,
                        "stolen_list":stolen_list,
                        "send_plate_data_to_client": send_plate_data_to_client,
                        "websocket": websocket, "plate_data": plate_data,
                        "deviceId": deviceId,
                        "source": streamid,
                        "device": "cpu" ,
                        "ocr": ocr,
                        "vid_stride": vid_stride,
                        "conf_thres": conf_thres,
                        "iou_thres": iou_thres,
                        "previous_frame":previous_frame
                        
                    }
            threading.Thread(target=run, args=(model, stride, names, pt),kwargs=kwargs).start()

    except Exception as e:
        print(f"Error in websocket connection: {e}")
        traceback.print_exc()
        log.error(f"Error in websocket connection: {e}")
        # Clean up any resources or notify clients if needed


async def check_websockets():
    while True:
        try:
            closed_websockets = []
            for deviceId, websocket in web_socket_data.items():
                if websocket.closed:
                    closed_websockets.append(deviceId)

            for deviceId in closed_websockets:
                del web_socket_data[deviceId]
                # #empty device id
                # plate_data[deviceId]=[]
                # stolen_list_number_plate[deviceId]=[]

                # print(web_socket_data)
                # Perform any necessary cleanup or notify clients about the closed websocket

            await asyncio.sleep(5)  # Adjust the interval based on your needs
        except Exception as e:
            print("the error in deviceID",deviceId)
            print(e)


async def main():
    websocket_server = serve(testHandler, server, port)
    websocket_checker = check_websockets()

    await asyncio.gather(websocket_server, websocket_checker)


asyncio.run(main())
